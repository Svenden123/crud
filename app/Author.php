<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $fillable = [
        'name', 'surname','residence',
    ];

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function votes()
    {
        return $this->hasMany('App\Vote');
    }
}
