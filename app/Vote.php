<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    protected $fillable = [
        'is_like', 'article_id','author_id',
    ];

    public function author()
    {
        return $this->hasOne('App\Author');
    }

    public function article()
    {
        return $this->hasOne('App\Article');
    }
}
