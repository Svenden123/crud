<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = [
        'title', 'description','author_id',
    ];
    public function author()
    {
        return $this->hasOne('App\Author');
    }

    public function votes()
    {
        return $this->hasOne('App\Vote');
    }
}
